const Poly = {
    handlers: [],
    modules: {}
};

Poly.extend = (module) => {
    if (module.extend && module.parse && module.module_id) {
        Poly.modules[module.module_id] = module;
        Poly.handlers.push(module.parse);
    } else throw new Error("Module not properly defined!");
}

Poly.getModule = id => Poly.modules[id];
Poly.read = path => fs.readFileSync(path).toString();

Poly.edit = (source,filename) =>
    Poly.handlers.reduce((source,handler) => {
        let out = handler(source, filename);
        if(out && out.length) source = out;
        
        return source;
    }, source);

const compileFunc = module.constructor.prototype._compile;
module.constructor.prototype._compile = function(content, filename) {
    let newContent = Poly.edit(content, filename);
    compileFunc.call(this, newContent, filename);
};

module.exports = Poly;
const babel = require("@babel/core");
require("babel-polyfill");

const BABEL_OPTS = {
    plugins: [
        "@babel/plugin-syntax-dynamic-import",
        "@babel/plugin-syntax-import-meta",
        ["@babel/plugin-proposal-class-properties", { "loose": false }],
        "@babel/plugin-proposal-json-strings"
    ],
    presets: ["@babel/env"]
};

// Adds a bridge for Babel transpilation
//Unlike something like create-react-app, this allows you to also compile ES6 node_modules, as long as you specify them in the includes opt
exports = module.exports = ({ includes = [], babelOpts = {} }) => {
    let mod = {
        module_id: "babel",
        extend: () => null
    };


    if(includes.length) {
        let regex = new RegExp(`node_modules[\\\\\\\/](${includes.join("|")})`, "gi");
        const notIncluded = filename =>
            !regex.test(filename);
        
        mod.parse = (code, filename) => {
            if(/node_modules/g.test(filename) && notIncluded(filename)) {
                return code; //We don't want to
            } else return babel.transformSync(code, Object.assign({}, BABEL_OPTS, babelOpts)).code;
        }
    } else {
        mod.parse = (code, filename) => {
            if(/node_modules/g.test(filename)) {
                return code;
            } else return babel.transformSync(code, Object.assign({}, BABEL_OPTS, babelOpts)).code;
        }
    }

    return mod;
}
const path = require("path");

// Adds support for ES6-style imports (does not support aliases)
exports = module.exports = (rootDir) => {
    let parseImport = file => {
        if(file.substring(0, 2) === "~/") {
            return path.resolve(`${rootDir}/${file.substring(2)}`).replace(/\\+/g, "/");
        } else return file;
    }

    return {
        module_id: "root-level-import",
        extend: () => null,
        parse: code => code
            .replace(/from[\t ]+["'](~\/[^"']*)["'][\t ]*;[\t ]*$/gmi, (_, file) => `from "${parseImport(file)}";`)
            .replace(/require[\t ]*\(["'](~\/[^"']*)["']\)/gmi, (_, file) => `require("${parseImport(file)}")`)
            .replace(/import[\t ]*\(["'](~\/[^"']*)["']\)/gmi, (_, file) => `import("${parseImport(file)}")`)
    }
};